# -*- coding: utf-8 -*-
'''
# @Time    : 2018/12/6 10:19
# @Author  : Wang Qiang
# @FileName: main.py
# @Software: PyCharm
'''
import random
import xlwt
import xlrd
import copy
import matplotlib.pyplot as plt
import math
import numpy as np
# 拟合幂律分布，并求出幂律指数，可视化显示
# f(x) = cx^(-r)
# log[f(x)] = -rlog(x) + log(c)
# y = -rx + c
def DataFitAndVisualization(X, Y, saveFileName):
    # 模型数据准备
    zero = [i for i,x in enumerate(Y) if x == 0]
    i = 0
    for z in zero:
        del X[z - i]
        del Y[z - i]
        i += 1
    X = np.log10(X)  # 对X，Y取双对数
    Y = np.log10(Y)
    X_parameter = []
    Y_parameter = []
    for single_square_feet, single_price_value in zip(X, Y):
        X_parameter.append([float(single_square_feet)])
        Y_parameter.append(float(single_price_value))

    # 模型拟合
    regr = linear_model.LinearRegression()
    regr.fit(X_parameter, Y_parameter)
    # 模型结果与得分
    print('Coefficients: \n', regr.coef_, )
    print("Intercept:\n", regr.intercept_)
    # The mean square error
    print("Residual sum of squares: %.8f"
          % np.mean((regr.predict(X_parameter) - Y_parameter) ** 2))  # 残差平方和

    # 可视化
    lb = "y = " + str(regr.coef_[0]) + "x + " + str(regr.intercept_)
    plt.title(saveFileName + " Degree Distribution Log Data")
    plt.scatter(X_parameter, Y_parameter, s = 10, color='c',alpha = 0.75)
    plt.plot(X_parameter, regr.predict(X_parameter), color='coral', linewidth=2, label= lb)
    plt.legend()
    plt.xlabel("Degree")
    plt.ylabel("Frequency")
    plt.savefig("./figure/" + saveFileName + " Degree Distribution Fitting.png")
    plt.show()

def creatExcel(fileName):
    # 创建一个Workbook对象，这就相当于创建了一个Excel文件
    book = xlwt.Workbook(encoding='utf-8', style_compression=0)
    '''
    Workbook类初始化时有encoding和style_compression参数
    encoding:设置字符编码，一般要这样设置：w = Workbook(encoding='utf-8')，就可以在excel中输出中文了。
    默认是ascii。当然要记得在文件头部添加：
    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    style_compression:表示是否压缩，不常用。
    '''
    # 创建一个sheet对象，一个sheet对象对应Excel文件中的一张表格。
    # 在电脑桌面右键新建一个Excel文件，其中就包含sheet1，sheet2，sheet3三张表
    sheet = book.add_sheet('test', cell_overwrite_ok=True)
    # 其中的test是这张表的名字,cell_overwrite_ok，表示是否可以覆盖单元格，其实是Worksheet实例化的一个参数，默认值是False
    # 向表test中添加数据
    sheet.write(0, 0, '仿真时间（s）')  # 其中的'0-行, 0-列'指定表中的单元，'EnglishName'是向该单元写入的内容
    sheet.write(0, 1, '数据包大小（bytes）')
    sheet.write(0, 2, '带宽（Mbit）')
    sheet.write(0, 3, '统计时间间隔（s）')
    sheet.write(0, 4, '分布（a.指数/b.幂率）')
    sheet.write(0, 5, '分布参数（Lambda^-1/minInterval(s),maxInterval(s),Alpha）')
    sheet.write(0, 6, '应用层最大包长（bytes）')

    # 最后，将以上操作保存到指定的Excel文件中
    book.save(fileName)  # 在字符串前加r，声明为raw字符串，这样就不会处理其中的转义了。否则，可能会报错

def readExcelSimulation(fileName):
    book = xlrd.open_workbook(fileName)  # 得到Excel文件的book对象，实例化对象
    sheet0 = book.sheet_by_index(0)  # 通过sheet索引获得sheet对象
    # print("1、sheet0: ", sheet0)
    sheet_name = book.sheet_names()[0]  # 获得指定索引的sheet表名字
    print("sheet name: ", sheet_name)
    sheet1 = book.sheet_by_name(sheet_name)  # 通过sheet名字来获取，当然如果知道sheet名字就可以直接指定
    nrows = sheet0.nrows  # 获取行总数
    print("Number of rows: ", nrows)
    # 循环打印每一行的内容
    # for i in range(nrows):
    #     print(sheet1.row_values(i))
    ncols = sheet0.ncols  # 获取列总数
    print("Number of cols: ", ncols)
    # row_data = sheet0.row_values(0)  # 获得第1行的数据列表
    # print(row_data)
    # col_data = sheet0.col_values(0)  # 获得第1列的数据列表
    # print("5、col_data: ", col_data)
    # 通过坐标读取表格中的数据
    # cell_value1 = sheet0.cell_value(0, 0)
    # print("6、cell_value1: ", cell_value1)
    # cell_value2 = sheet0.cell_value(0, 1)
    # print("7、cell_value2: ", cell_value2)
    for i in range(nrows):
        if i == 0:
            continue
        else:
            row = sheet1.row_values(i)
            if len(row) < ncols or '' in row:
                print("Error: Line %d has blank!"%i)
            else:
                print("Line %d simulation starts." % i)
                if row[1] > row[6]:# 大的数据包进行分包，udp + ip包头28bytes，以太网26bytes
                    packetSize = math.floor(row[1]/row[6]) * (row[6] + 28 + 26) + (row[1] - math.floor(row[1]/row[6])*row[6] + 28 + 26)
                    packetTimeConsuming = math.floor(row[1]/row[6]) * (row[6] + 28 + 26) * 8 / (row[2] * 1024 * 1024)
                    packetTimeConsuming += (row[1] - math.floor(row[1]/row[6])*row[6] + 28 + 26) * 8 / (row[2] * 1024 * 1024)
                else:
                    packetSize = row[1] + 28 + 26
                    packetTimeConsuming = row[1] * 8 / (row[2] * 1024 * 1024)
                TimeConsuming = []
                TimeConsumingSum = 0
                while TimeConsumingSum < row[0]:
                    TimeConsuming.append(packetTimeConsuming)
                    TimeConsumingSum += TimeConsuming[-1]
                    packetSendInterval = 0
                    if row[4] == 'a':
                        packetSendInterval = random.expovariate(1 / row[5]) - packetTimeConsuming
                    elif row[4] == 'b':
                        if 'transform_value' not in locals().keys():
                            # 帕累托分布（Pareto distribution）， alpha 是形状参数。
                            parameter = row[5].split(',')
                            parameter = [float(x) for x in parameter]
                            source = [random.paretovariate(parameter[2]) for _ in range(1, 30000)]
                            k = (-parameter[0] - (-parameter[1])) / (max(source) - min(source))
                            transform_value = [-(-parameter[1] + k * (x - min(source))) for x in source]
                            print("packet send min interval %fs." % min(transform_value))
                            print("packet send max interval %fs." % max(transform_value))
                        packetSendInterval = random.choice(transform_value) - packetTimeConsuming
                    else:
                        print("Error: Line %d - unknown distribution!" % i)
                    if packetSendInterval <= 0:
                        packetSendInterval = 0
                    TimeConsuming.append(packetSendInterval)
                    TimeConsumingSum += TimeConsuming[-1]
                time = []
                lastTime = 0
                nowTime = 0
                tempTime = 0
                hasPacket = True
                packetSend = []
                tempSend = 0
                j = 0
                intervalSum = 0
                TimeConsumingSum = 0
                interval = []
                intervalTime = []
                count = 0
                countInterval = 0
                while j < len(TimeConsuming):
                    if j > count:
                        if count > 0 and count % (round(len(TimeConsuming) / 100)) == 0:
                            print('simulation time: %f' % time[-1])
                        TimeConsumingSum += TimeConsuming[j]
                        count = j
                    if (j % 2) == 0:
                        hasPacket = False
                    else:
                        if j > countInterval:
                            intervalSum += TimeConsuming[j]
                            interval.append(TimeConsuming[j])
                            intervalTime.append(TimeConsumingSum)
                            countInterval = j
                        hasPacket = True
                    if tempTime > 0:
                        nowTime += tempTime
                        if nowTime - lastTime >= row[3]:
                            addTime = tempTime - (nowTime - lastTime - row[3])
                            nowTime += -tempTime + addTime
                            if hasPacket:
                                tempSend += addTime * packetSize * 8 / packetTimeConsuming
                            packetSend.append(tempSend)
                            tempSend = 0
                            tempTime = tempTime - addTime
                            lastTime = copy.deepcopy(nowTime)
                            time.append((len(time) + 1) * row[3])
                            continue
                        if hasPacket:
                            tempSend += tempTime * packetSize * 8 / packetTimeConsuming
                    nowTime += TimeConsuming[j]
                    if nowTime - lastTime >= row[3]:
                        addTime = TimeConsuming[j] - (nowTime - lastTime - row[3])
                        nowTime += -TimeConsuming[j] + addTime
                        if not hasPacket:
                            tempSend += addTime * packetSize * 8 / packetTimeConsuming
                        packetSend.append(tempSend)
                        tempSend = 0
                        tempTime = TimeConsuming[j] - addTime
                        lastTime = copy.deepcopy(nowTime)
                        j += 1
                        time.append((len(time) + 1) * row[3])
                        continue
                    j += 1
                    tempTime = 0
                    if not hasPacket:
                        tempSend += packetSize * 8
                traffic = [x/(row[3] * 1024 * 1024) for x in packetSend]
                figName = './figure/Traffic simulationTime_' + str(row[0]) + ' packetSize_' + str(row[1]) + ' bandWidth_' + str(row[2]) \
                + ' StatisticalInterval_' + str(row[3]) + ' distribution_' + str(row[4]) + ' parameter_' + str(row[5]) + ' maxSize_' + str(row[6]) + '.png'
                plt.figure()
                plt.plot(time,traffic, linewidth = '0.08')
                plt.title('Data Flow Curve')
                plt.xlabel('simulation time(s)')
                plt.ylabel('Data traffic(Mbps)')
                plt.savefig(figName)
                # plt.show() # 如果想直接看到图则打开此注释，图片已经保存到figure文件夹下
                figName = './figure/TrafficHist simulationTime_' + str(row[0]) + ' packetSize_' + str(row[1]) + ' bandWidth_' + str(row[2]) \
                          + ' StatisticalInterval_' + str(row[3]) + ' distribution_' + str(row[4]) + ' parameter_' + str(row[5]) + ' maxSize_' + str(row[6]) + '.png'
                plt.figure()
                plt.hist(traffic, 200)
                plt.yscale('log')
                plt.title('Data Flow Hist')
                plt.xlabel('Data traffic(Mbps)')
                plt.ylabel('Frequency')
                plt.savefig(figName)
                # plt.show() # 如果想直接看到图则打开此注释，图片已经保存到figure文件夹下
                figName = './figure/Interval simulationTime_' + str(row[0]) + ' packetSize_' + str(row[1]) + ' bandWidth_' + str(row[2]) \
                          + ' StatisticalInterval_' + str(row[3]) + ' distribution_' + str(row[4]) + ' parameter_' + str(row[5]) + ' maxSize_' + str(row[6]) + '.png'
                plt.figure()
                plt.plot(intervalTime, interval, linewidth = '0.5')
                plt.title('Packet Send Interval Curve')
                plt.xlabel('simulation time(s)')
                plt.ylabel('packet send interval(s)')
                plt.savefig(figName)
                # plt.show() # 如果想直接看到图则打开此注释，图片已经保存到figure文件夹下
                figName = './figure/Distribution simulationTime_' + str(row[0]) + ' packetSize_' + \
                          str(row[1]) + ' bandWidth_' + str(row[2]) \
                          + ' StatisticalInterval_' + str(row[3]) + ' distribution_' + \
                          str(row[4]) + ' parameter_' + str(row[5]) + ' maxSize_' + str(row[6]) + '.png'
                plt.figure()
                plt.title("Packet Send Interval Hist")
                plt.xlabel("Packet Send Interval(s)")
                plt.ylabel("Frequency")
                plt.hist(interval, 100)
                plt.savefig(figName)
                # plt.show()
                print("Line %d simulation ends." % i)
                print("Average send interval: %fs." % ((intervalSum / math.ceil(len(TimeConsuming)/2)) + packetTimeConsuming))
                print("Average flow rate: %fMbps." % (math.ceil(len(TimeConsuming)/2) * row[1] * 8 / (row[0] * 1024 * 1024)))



if __name__ == '__main__':
    # creatExcel('test.xls')
    readExcelSimulation('test.xls')
